# Tool for testing containerized systems

**If you see this note and it's 2023, feel free to nuke the repository. It's probably useless**

---

* **not finished or ready for production use**
* extracted from [knot-resolver-manager](https://gitlab.nic.cz/knot/knot-resolver-manager) before purging it there
* written in Python with Poetry as the dependency manager
* directory structure
  * `containers` - contains container definitions managed by the command `poe container`
  * `distro` - contains old testing packaging for knot-resolver, controlled by `poe package`
  * `integration` - contains the definition of tests and the test runner, invoked by `poe integration`
  * `config` - contains config files