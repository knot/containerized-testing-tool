# Crash resilience integration test

**Goal:** Test that even in cases when the manager crashes and restarts, the operation on the resolver is not interrupted.