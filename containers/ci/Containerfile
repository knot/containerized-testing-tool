FROM registry.nic.cz/labs/lxc-gitlab-runner/fedora-34:podman

# Install Python and NodeJS
RUN dnf install -y python3.6 nodejs python3-gobject pkg-config cairo-devel gcc python3-devel gobject-introspection-devel cairo-gobject-devel which \
  && dnf clean all

# Install Poetry
RUN python3 -m pip install -U pip \
  && curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 - \
  && source $HOME/.poetry/env \
  # not exactly required, but helpful
  && python3 -m pip install poethepoet

# force Poetry to use local .venv/ directory that we can cache
ENV POETRY_NO_INTERACTION=1 \
  # python:
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PYTHONDONTWRITEBYTECODE=1


# How does this work?
# ===================
#
# NPM dependencies are installed globally. There is no problem with that.
#
# Python dependencies are however installed into a virtualenv created by Poetry. Why you might ask?
# Because we can't change the default python interpreter without virtualenv. This creates a problem,
# that the virtualenv is created for a directory different than the one, where CI will run.
#
# Yup, that's a slight issue, that has to be fixed before running anything. This migration step is however
# quick. It's just copying files locally and there's not a ton of them. This virtualenv migration step is
# therefore done every time a CI job starts.
#
# How does it speed up CI?
# ========================
#
# We do not have to install the dependencies every single time. They are cached in the container itself and 
# we can rebuild it only when it's definition or the list of dependencies changes.

COPY pyproject.toml package.json .
RUN source $HOME/.poetry/env \
  && poetry config --list \
  && poetry env use $(which python3.6) \
  && poetry env info \
  && poetry install --no-interaction --no-ansi \
  && npm install -g $(python -c "import json; print(*(k for k in json.loads(open('package.json').read())['dependencies']))")