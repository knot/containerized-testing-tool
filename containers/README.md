# Container definition files

All containers build configurations evolve from the `dev` container. If you want to change something, please do it mainly there. We consider **the `dev` container as a reference container**

## Naming

All containers defined here are named `knot-manager`. The directory name is the container's tag. So, for example - `knot-manager:dev` is the reference development container.

## Building

```sh
for tag in 
```